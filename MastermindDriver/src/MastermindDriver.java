public class MastermindDriver {

    public static void main(String[] args){
        Game g = new Game(true);
        g.runGames();
        g = new Game(false);
        g.runGames();
    }

}