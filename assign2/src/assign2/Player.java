package assign2;

import java.util.ArrayList;

/**
Player class that holds Guesses and History
Solves EE422C programming assignment #2
@author Biraj Shrestha, Eric Van Dyk 
*/

public class Player
{
	
	int numGuesses;
	ArrayList<String> guessHistory = new ArrayList<String>();
	Code guess;
	
	public Player()
	{
		numGuesses = 0;
		
	}
	
	
	/**
	   Returns number of guesses
	   @param void
	   @return int value of number of guesses
	*/
	public int getNumGuesses()
	{
		return numGuesses;
	}
	
	/**
	   Prints history of all guesses
	   @param void
	   @return void
	*/
	public void printHistory()
	{
		System.out.println("History of Guesses: ");
		for(int k = 0; k < guessHistory.size(); k++)
		{
			System.out.println(guessHistory.get(k));
		}
	}
	
	/**
	   Places code into code history
	   @param Code to be added into history
	   @return void
	*/
	public void setHistory(Code currGuess)
	{
		StringBuilder toAdd = currGuess.code;
		guessHistory.add(toAdd.toString());
	}
}
