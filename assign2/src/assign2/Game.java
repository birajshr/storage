package assign2;

import java.util.Scanner;

/**
Player class sets up Game and runs program
Solves EE422C programming assignment #2
@author Biraj Shrestha, Eric Van Dyk 
*/
public class Game
{
	int maxNumGuesses;
	int blackPegs;
	int whitePegs;
	boolean testMode;
	Player newPlayer = new Player();
	Colors allowed = new Colors();
	Code gameCode;// = new Code();
	Scanner kb;// = new Scanner(System.in);
	
	public Game()
	{
		maxNumGuesses = 12;
		blackPegs = 0;
		whitePegs = 0;
	}
		
	/**
	   Runs testMode
	   @param boolean value of test
	   @return void
	*/
	public void inTestMode(boolean isTest)
	{
		testMode = isTest;
	}
	
	/**
	   Method to run game
	   @param void
	   @return void
	*/
	public void runGame()
	{
		gameCode = new Code();
		gameCode.randomize(allowed, gameCode.testMode);
		Code guess = new Code();
		boolean valid;
		boolean canContinue = true;
		do
		{
			guess = getInput();
			valid = isValidGuess(guess);
			String a = "history";
			String b = guess.code.toString();
			if(b.equals(a))
			{
				newPlayer.printHistory();
			}
			else if(valid == true)
			{
				newPlayer.numGuesses += 1;
				newPlayer.setHistory(guess);
				boolean temp = gameCode.compareGuess(guess);
				if(temp == false)
				{
					System.out.println("Result : " + gameCode.blackPegs + " Black Pegs, "
							+ gameCode.whitePegs + " White Pegs.");
					gameCode.blackPegs = 0;
					gameCode.whitePegs = 0;
				}
				else
				{
					canContinue = false;
					System.out.println("Congratulations, you guessed correctly! You Win!");
				}
				
				
				if(this.maxNumGuesses - newPlayer.numGuesses == 0)
				{
					canContinue = false;
					System.out.println("You have have run out of guesses. You Lose!");
				}
			}
			else
			{
				System.out.println("Invalid Guess. Please try again.");
			}
		}while(canContinue == true);
	}
	
	/**
	   Gets input from user
	   @param void
	   @return Code class of Input
	*/
	public Code getInput()
	{
		int guessesLeft = maxNumGuesses - newPlayer.getNumGuesses();
		kb = new Scanner(System.in);
		StringBuilder guess = new StringBuilder();
		System.out.println(
				"You have " + guessesLeft + " guesses left.\n"
				+ "What is your next guess?\n"
				+ "Type in the characters for your guess and press enter.\n"
				+ "Enter guess: ");
		guess.append(kb.next());
		Code temp = new Code();
		temp.setCode(guess);
		return temp;
	}

	/**
	   Checks if Code is valid
	   @param Code to be checked against
	   @return boolean whether if input is valid
	*/
	public boolean isValidGuess(Code currGuess)
	{
		String a = "history";
		String b = currGuess.code.toString();
		if(b.equals(a))
		{
			return false;
		}		
		if(currGuess.getLength() != gameCode.getLength())
		{
			System.out.println("Incorrect guess length.");
			return false;
		}
		for(int i = 0; i < currGuess.getLength(); i++)
		{			
			if(allowed.included(currGuess,i) == false)
			{
				return false;
			}
		}
		return true;
	}
		
	/**
	   Sets up value for Black Peg
	   @param int value of peg
	   @return void
	*/
	public void setBlackPegs(int i)
	{
		this.blackPegs = i;
	}
	
	/**
	   Sets up value for White Peg
	   @param int value of peg
	   @return void
	*/
	public void setWhitePegs(int i)
	{
		this.whitePegs = i;
	}
}
