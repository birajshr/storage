package assign2;
import java.io.*;
import java.util.*;


/**
Driver class creates and runs a game
Solves EE422C programming assignment #2
@author Biraj Shrestha, Eric Van Dyk
@section Friday 9-10:30AM
*/
public class assign2 {
	
	public static void main(String args[])
	{ 	
		Scanner kb = new Scanner(System.in);
		System.out.println("Welcome to Mastermind.  Here are the rules.\n\n"
				+ "This is a text version of the classic board game Mastermind.\n"
				+ "The computer will think of a secret code. The code consists of 4 colored pegs.\n"
				+ "The pegs MUST be one of six colors: blue, green, orange, purple, red, or yellow. A color may\n"
				+ "appear more than once in the code. You try to guess what colored pegs are in the code and what\n"
				+ "order they are in.   After you make a valid guess the result (feedback) will be displayed.\n"
				+ "he result consists of a black peg for each peg you have guessed exactly correct (color and\n"
				+ "position) in your guess.  For each peg in the guess that is the correct color, but is out of position,\n"
				+ "you get a white peg.  For each peg, which is fully incorrect, you get no feedback.\n\n"
				+ "Only the first letter of the color is displayed. B for Blue, R for Red, and so forth.\n"
				+ "When entering guesses you only need to enter the first character of each color as a capital letter.\n\n"
				+ "You have 12 guesses to figure out the secret code or you lose the game.  Are you ready to play?\n"
				+ "(Y/N):");
		
		String userInput = kb.next();
		
		if(userInput.equals("y") || userInput.equals("Y"))
		{
			do
			{
				Game play = new Game();
				play.inTestMode(false);
				play.runGame();
				System.out.print("\n\nWould you like to play again? [Y/N]");
				userInput = kb.next();
			}while(userInput.equals("Y"));
		}
	}

}
